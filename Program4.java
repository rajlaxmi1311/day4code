import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter starting range : ");
		int startRange = Integer.parseInt(br.readLine());

		System.out.print("Enter ending range : ");
		int endRange = Integer.parseInt(br.readLine());

		int sum =0;
		for(int i= startRange; i< endRange; i++){
		
			sum= sum +i;

		}
		System.out.println("sum is : " + sum);
	}
}
